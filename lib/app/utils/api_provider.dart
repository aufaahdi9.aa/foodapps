import 'package:dio/dio.dart';
import 'package:food_app/app/models/IngredientResponse.dart';
import 'package:food_app/app/models/food.dart';

class ApiProvider {
  final Dio _dio = Dio();
  final String _baseUrl = 'https://www.themealdb.com/api/json/v1/1';

  Future<FoodModel> fetchFoodList() async {
    try {
      Response response = await _dio.get('$_baseUrl/filter.php?a=Canadian');
      return FoodModel.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return FoodModel.withError("Data not found / Connection issue");
    }
  }

  Future<IngredientResponse> fetchIngredient(String id) async {
    try {
      Response response = await _dio.get("$_baseUrl/lookup.php?i=$id");
      print(response.data);
      return IngredientResponse.fromJson(response.data);
    } catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return IngredientResponse.withError("Data not found / Connection issue");
    }
  }
}
