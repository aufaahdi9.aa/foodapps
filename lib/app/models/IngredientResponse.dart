import 'ingredient.dart';

class IngredientResponse {
  final List<Ingredients> meals;
  final String error;

  IngredientResponse(this.meals, this.error);

  IngredientResponse.fromJson(Map<String, dynamic> json)
      : meals = (json["meals"] as List)
            .map((i) => new Ingredients.fromJson(i))
            .toList(),
        error = "";

  IngredientResponse.withError(String errorValue)
      : meals = [],
        error = errorValue;
}
