class FoodModel {
  List<Meal> meals;
  String error;

  FoodModel({this.meals});

  FoodModel.withError(String errorMessage) {
    error = errorMessage;
  }

  FoodModel.fromJson(Map<String, dynamic> json) {
    if (json['meals'] != null) {
      meals = <Meal>[];
      json['meals'].forEach((v) {
        meals.add(new Meal.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.meals != null) {
      data['meals'] = this.meals.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Meal {
  String strMeal;
  String strMealThumb;
  String idMeal;
  bool isFavorite = false;

  Meal({this.strMeal, this.strMealThumb, this.idMeal, this.isFavorite});

  Meal.fromJson(Map<String, dynamic> json) {
    strMeal = json['strMeal'];
    strMealThumb = json['strMealThumb'];
    idMeal = json['idMeal'];
    isFavorite = json['isfavorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['strMeal'] = this.strMeal;
    data['strMealThumb'] = this.strMealThumb;
    data['idMeal'] = this.idMeal;
    data['isfavorite'] = this.isFavorite;
    return data;
  }
}
