import 'package:flutter/material.dart';
import 'package:food_app/app/screens/home.dart';

import 'Favorite.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        children: <Widget>[
          FoodPage(),
          Favorite(),
        ],
        // If you want to disable swiping in tab the use below code
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController,
      ),
      // backgroundColor: backgroundSlate,
      bottomNavigationBar: Container(
        color: Colors.white,
        child: TabBar(
          labelColor: Colors.blue,
          unselectedLabelColor: Colors.grey,
          labelStyle: TextStyle(fontSize: 10.0),
          //For Indicator Show and Customization
          indicatorColor: Colors.black54,
          tabs: tabs(),
          controller: _tabController,
          onTap: (index) {
            setState(() => currentIndex = index);
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  List<Widget> tabs() {
    return <Widget>[
      Tab(
        icon: Icon(Icons.home,
            color: currentIndex == 0 ? Colors.blue : Colors.grey),
        text: 'Beranda',
      ),
      Tab(
        icon: Icon(Icons.favorite,
            color: currentIndex == 1 ? Colors.blue : Colors.grey),
        text: 'Telusuri',
      ),
    ];
  }
}
