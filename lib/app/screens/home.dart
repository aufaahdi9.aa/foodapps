import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/app/blocs/food/food_bloc.dart';
import 'package:food_app/app/blocs/food/food_event.dart';
import 'package:food_app/app/models/food.dart';
import 'package:food_app/app/screens/detail.dart';
import 'package:food_app/data/db/item_db.dart';
import 'package:toast/toast.dart';

class FoodPage extends StatefulWidget {
  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  final FoodBloc _foodBloc = FoodBloc();

  bool isLike = false;
  List favorite = [];

  @override
  void initState() {
    _foodBloc.add(GetFoodList());
    AppDatabase().watchAllItem().forEach((element) {
      favorite.add(element);
      print(favorite);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Center(
                  child: Text(
                    'Food Recipes For You',
                    key: Key('header'),
                    style: TextStyle(
                        letterSpacing: 0.5,
                        color: Colors.black,
                        fontFamily: "Sans",
                        fontWeight: FontWeight.bold,
                        fontSize: 27.0),
                  ),
                ),
              ),
              flex: 1,
            ),
            Expanded(
              child: _buildListFood(),
              flex: 3,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListFood() {
    return Container(
      margin: EdgeInsets.all(8.0),
      child: BlocProvider(
        create: (_) => _foodBloc,
        child: BlocListener<FoodBloc, FoodState>(
          listener: (context, state) {
            if (state is FoodError) {
              Toast.show("Erorr data", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            }
          },
          child: BlocBuilder<FoodBloc, FoodState>(
            builder: (context, state) {
              if (state is FoodInitial) {
                return _buildLoading();
              } else if (state is FoodLoading) {
                return _buildLoading();
              } else if (state is FoodLoaded) {
                return _buildCard(context, state.foodModel);
              } else if (state is FoodError) {
                return Container(
                  child: Text(
                    'Error Data',
                    key: Key('eror'),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  Widget _buildCard(BuildContext context, FoodModel model) {
    return GridView.builder(
      key: Key('list meals'),
      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 17.0,
        childAspectRatio: 0.480,
        crossAxisCount: 2,
      ),
      itemCount: model.meals.length,
      itemBuilder: (context, index) {
        return InkWell(
          key: Key('go detail'),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailFood(
                          id: model.meals[index].idMeal,
                        )));
          },
          child: Container(
            key: Key('card'),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFF656565).withOpacity(0.15),
                    blurRadius: 4.0,
                    spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
                  )
                ]),
            child: Wrap(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Hero(
                      tag: "hero-grid-${model.meals[index].idMeal}",
                      child: Material(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(PageRouteBuilder(
                                opaque: false,
                                pageBuilder: (BuildContext context, _, __) {
                                  return new Material(
                                    color: Colors.black54,
                                    child: Container(
                                      padding: EdgeInsets.all(30.0),
                                      child: InkWell(
                                        child: Hero(
                                            tag:
                                                "hero-grid-${model.meals[index].idMeal}",
                                            child: Image.network(
                                              model.meals[index].strMealThumb,
                                              width: 300.0,
                                              height: 300.0,
                                              alignment: Alignment.center,
                                              fit: BoxFit.contain,
                                            )),
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                      ),
                                    ),
                                  );
                                },
                                transitionDuration:
                                    Duration(milliseconds: 500)));
                          },
                          child: Container(
                            key: Key('img'),
                            height: MediaQuery.of(context).size.height / 3.3,
                            width: 200.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(7.0),
                                    topRight: Radius.circular(7.0)),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        model.meals[index].strMealThumb),
                                    fit: BoxFit.cover)),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 10, left: 15.0, right: 15.0),
                      child: Text(
                        model.meals[index].strMeal,
                        key: Key('title'),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: "Sans",
                            fontSize: 14.0,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 10, left: 15.0, right: 15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Icon(
                                  Icons.thumb_up,
                                  color: Colors.grey,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  likes(int.parse(model.meals[index].idMeal))
                                      .toString()
                                      .substring(0, 3),
                                  key: Key('id'),
                                  style: TextStyle(
                                      letterSpacing: 0.5,
                                      color: Colors.black,
                                      fontFamily: "Sans",
                                      fontWeight: FontWeight.w700,
                                      fontSize: 15.0),
                                ),
                              ],
                            ),
                          ),
                          IconButton(
                              icon: Icon(
                                Icons.favorite,
                                color: model.meals[index].isFavorite == true
                                    ? Colors.red
                                    : Colors.grey,
                              ),
                              onPressed: () {
                                if (model.meals[index].isFavorite == true) {
                                  model.meals[index].isFavorite = false;
                                  print(model.meals[index].isFavorite);
                                  AppDatabase().deleteItem(Item(
                                      id: int.parse(model.meals[index].idMeal),
                                      productImg:
                                          model.meals[index].strMealThumb,
                                      productName: model.meals[index].strMeal,
                                      productCode: model.meals[index].idMeal));
                                  setState(() {});
                                } else {
                                  model.meals[index].isFavorite = true;
                                  AppDatabase().insertNewItem(Item(
                                    id: int.parse(model.meals[index].idMeal),
                                    productCode: model.meals[index].idMeal,
                                    productName: model.meals[index].strMeal,
                                    productImg: model.meals[index].strMealThumb,
                                  ));
                                  setState(() {});
                                }
                              })
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  int likes(int id) {
    return id + Random().nextInt(100);
  }

  Widget _buildLoading() => Center(child: CircularProgressIndicator());
}
