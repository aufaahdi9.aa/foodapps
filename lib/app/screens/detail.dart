import 'package:flutter/material.dart';
import 'package:food_app/app/blocs/Ingredient/ingredient_bloc.dart';
import 'package:food_app/app/models/IngredientResponse.dart';
import 'package:food_app/app/models/ingredient.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class DetailFood extends StatefulWidget {
  final String id;
  DetailFood({this.id});
  @override
  _DetailFoodState createState() => _DetailFoodState();
}

class _DetailFoodState extends State<DetailFood> {
  List<IngredientResponse> ingredient = [];

  @override
  void initState() {
    ingredientVideosBloc..getIngredientVideos(widget.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: StreamBuilder<IngredientResponse>(
        stream: ingredientVideosBloc.subject.stream,
        builder: (context, AsyncSnapshot<IngredientResponse> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return _buildErrorWidget(snapshot.data.error);
            }
            return _buildHomeWidget(snapshot.data);
          } else if (snapshot.hasError) {
            return _buildErrorWidget(snapshot.error);
          } else {
            return _buildLoadingWidget();
          }
        },
      ),
    ));
  }

  Widget _buildHomeWidget(IngredientResponse ingredient) {
    List<Ingredients> ingredients = ingredient.meals;
    print('testing' + ingredients.length.toString());
    if (ingredients.length == 0) {
      Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "No More Data",
                  style: TextStyle(color: Colors.black45),
                )
              ],
            )
          ],
        ),
      );
    } else {
      return SlidingUpPanel(
        panel: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: [
              Center(
                child: Text(
                  "Instruction",
                  style: TextStyle(
                      letterSpacing: 0.5,
                      color: Colors.black,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.w700,
                      fontSize: 18.0),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                ingredients[0].strInstructions,
                textAlign: TextAlign.center,
                style: TextStyle(
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontFamily: "Sans",
                    fontSize: 14.0),
              ),
            ],
          ),
        ),
        collapsed: Container(
          color: Colors.blueGrey,
          child: Column(
            children: [
              Icon(
                Icons.keyboard_arrow_up,
                size: 50,
                color: Colors.white,
              ),
              Text(
                "Instruction",
                style: TextStyle(
                    letterSpacing: 0.5,
                    color: Colors.white,
                    fontFamily: "Sans",
                    fontWeight: FontWeight.w700,
                    fontSize: 18.0),
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            Expanded(
              flex: 2,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(ingredients[0].strMealThumb),
                        fit: BoxFit.cover)),
              ),
            ),
            Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: ListView(
                    children: [
                      _title('Category : ' + ingredients[0].strCategory),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.location_on,
                              color: Colors.red,
                            ),
                            Text(
                              ingredients[0].strArea,
                              style: TextStyle(
                                  letterSpacing: 0.5,
                                  color: Colors.black,
                                  fontFamily: "Sans",
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18.0),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      _title('Ingredient'),
                      Container(
                        height: 50,
                        child: ListView(
                          padding: EdgeInsets.all(5),
                          scrollDirection: Axis.horizontal,
                          children: [
                            ingredients[0].strIngredient1 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient1,
                                    ingredients[0].strMeasure1),
                            ingredients[0].strIngredient2 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient2,
                                    ingredients[0].strMeasure2),
                            ingredients[0].strIngredient3 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient3,
                                    ingredients[0].strMeasure3),
                            ingredients[0].strIngredient4 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient4,
                                    ingredients[0].strMeasure4),
                            ingredients[0].strIngredient5 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient5,
                                    ingredients[0].strMeasure5),
                            ingredients[0].strIngredient6 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient6,
                                    ingredients[0].strMeasure6),
                            ingredients[0].strIngredient7 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient7,
                                    ingredients[0].strMeasure7),
                            ingredients[0].strIngredient8 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient8,
                                    ingredients[0].strMeasure8),
                            ingredients[0].strIngredient9 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient9,
                                    ingredients[0].strMeasure9),
                            ingredients[0].strIngredient10 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient10,
                                    ingredients[0].strMeasure10),
                            ingredients[0].strIngredient11 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient11,
                                    ingredients[0].strMeasure11),
                            ingredients[0].strIngredient12 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient12,
                                    ingredients[0].strMeasure12),
                            ingredients[0].strIngredient13 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient13,
                                    ingredients[0].strMeasure13),
                            ingredients[0].strIngredient14 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient14,
                                    ingredients[0].strMeasure14),
                            ingredients[0].strIngredient15 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient15,
                                    ingredients[0].strMeasure15),
                            ingredients[0].strIngredient16 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient16,
                                    ingredients[0].strMeasure16),
                            ingredients[0].strIngredient17 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient17,
                                    ingredients[0].strMeasure17),
                            ingredients[0].strIngredient18 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient12,
                                    ingredients[0].strMeasure18),
                            ingredients[0].strIngredient19 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient19,
                                    ingredients[0].strMeasure19),
                            ingredients[0].strIngredient20 == ''
                                ? Container()
                                : _content(ingredients[0].strIngredient20,
                                    ingredients[0].strMeasure20),
                          ],
                        ),
                      )
                    ],
                  ),
                ))
          ],
        ),
      );
    }
  }

  Widget _title(String title) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(
        title,
        style: TextStyle(
            letterSpacing: 0.5,
            color: Colors.black,
            fontFamily: "Sans",
            fontWeight: FontWeight.w700,
            fontSize: 18.0),
      ),
      SizedBox(
        height: 15,
      )
    ]);
  }

  Widget _content(String ingredient, String measure) {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: Container(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              ingredient + '/' + measure,
              style: TextStyle(
                  letterSpacing: 0.5,
                  color: Colors.black,
                  fontFamily: "Sans",
                  fontWeight: FontWeight.w700,
                  fontSize: 13.0),
            ),
          ),
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 7,
                offset: Offset(0, 2), // changes position of shadow
              ),
            ],
            color: Colors.grey[100]),
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Error occured: $error"),
      ],
    ));
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 25.0,
          width: 25.0,
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            strokeWidth: 4.0,
          ),
        )
      ],
    ));
  }
}
