import 'package:flutter/material.dart';
import 'package:food_app/data/db/item_db.dart';

class Favorite extends StatefulWidget {
  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width / 2,
              child: Center(
                child: Text(
                  'Your Favorite Food Recipe',
                  style: TextStyle(
                      letterSpacing: 0.5,
                      color: Colors.black,
                      fontFamily: "Sans",
                      fontWeight: FontWeight.bold,
                      fontSize: 27.0),
                ),
              ),
            ),
            flex: 1,
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
                stream: AppDatabase().watchAllItem(),
                builder: (context, AsyncSnapshot<List<Item>> snapshot) {
                  // print("ini cek data" + snapshot.data.toString());
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Center(child: CircularProgressIndicator());
                    default:
                      if (snapshot.hasError) {
                        return Center(child: Text('Error: ${snapshot.error}'));
                      } else {
                        if (snapshot.data.isEmpty) {
                          return Center(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.no_food,
                                color: Colors.blue,
                                size: 100,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                'Do not have a favorite food',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              )
                            ],
                          ));
                        } else {
                          /// if we want to do some data manipulation we
                          /// can do before it sending to a widget.
                          return GridView.builder(
                            gridDelegate:
                                new SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisSpacing: 10.0,
                              mainAxisSpacing: 17.0,
                              childAspectRatio: 0.480,
                              crossAxisCount: 2,
                            ),
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  // Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailFood(
                                  //   id: model.meals[index].idMeal,
                                  // )));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Color(0xFF656565)
                                              .withOpacity(0.15),
                                          blurRadius: 4.0,
                                          spreadRadius: 1.0,
//           offset: Offset(4.0, 10.0)
                                        )
                                      ]),
                                  child: Wrap(
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Hero(
                                            tag:
                                                "hero-grid-${snapshot.data[index].productCode}",
                                            child: Material(
                                              child: InkWell(
                                                onTap: () {
                                                  Navigator.of(context).push(
                                                      PageRouteBuilder(
                                                          opaque: false,
                                                          pageBuilder:
                                                              (BuildContext
                                                                      context,
                                                                  _,
                                                                  __) {
                                                            return new Material(
                                                              color: Colors
                                                                  .black54,
                                                              child: Container(
                                                                padding:
                                                                    EdgeInsets
                                                                        .all(
                                                                            30.0),
                                                                child: InkWell(
                                                                  child: Hero(
                                                                      tag:
                                                                          "hero-grid-${snapshot.data[index].productCode}",
                                                                      child: Image
                                                                          .network(
                                                                        snapshot
                                                                            .data[index]
                                                                            .productImg,
                                                                        width:
                                                                            300.0,
                                                                        height:
                                                                            300.0,
                                                                        alignment:
                                                                            Alignment.center,
                                                                        fit: BoxFit
                                                                            .contain,
                                                                      )),
                                                                  onTap: () {
                                                                    Navigator.pop(
                                                                        context);
                                                                  },
                                                                ),
                                                              ),
                                                            );
                                                          },
                                                          transitionDuration:
                                                              Duration(
                                                                  milliseconds:
                                                                      500)));
                                                },
                                                child: Container(
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height /
                                                      3.3,
                                                  width: 200.0,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                              topLeft: Radius
                                                                  .circular(
                                                                      7.0),
                                                              topRight: Radius
                                                                  .circular(
                                                                      7.0)),
                                                      image: DecorationImage(
                                                          image: NetworkImage(
                                                              snapshot
                                                                  .data[index]
                                                                  .productImg),
                                                          fit: BoxFit.cover)),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 10,
                                                left: 15.0,
                                                right: 15.0),
                                            child: Text(
                                              snapshot.data[index].productName,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontFamily: "Sans",
                                                  fontSize: 14.0,
                                                  fontWeight: FontWeight.w700),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 10,
                                                left: 15.0,
                                                right: 15.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Container(
                                                  child: Row(
                                                    children: [
                                                      Icon(
                                                        Icons.thumb_up,
                                                        color: Colors.grey,
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      Text(
                                                        '200',
                                                        style: TextStyle(
                                                            letterSpacing: 0.5,
                                                            color: Colors.black,
                                                            fontFamily: "Sans",
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 15.0),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                IconButton(
                                                    icon: Icon(
                                                      Icons.delete,
                                                      color: Colors.grey,
                                                    ),
                                                    onPressed: () {
                                                      setState(() {
                                                        AppDatabase()
                                                            .deleteItem(snapshot
                                                                .data[index]);
                                                      });
                                                    })
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      }
                  }
                }),
          ),
        ],
      ),
    ));
  }
}
