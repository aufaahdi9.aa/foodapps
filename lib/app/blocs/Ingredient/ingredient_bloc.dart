import 'package:flutter/material.dart';
import 'package:food_app/app/models/IngredientResponse.dart';
import 'package:food_app/app/utils/api_provider.dart';
import 'package:rxdart/rxdart.dart';

class IngredientsBloc {
  final ApiProvider _repository = ApiProvider();
  final BehaviorSubject<IngredientResponse> _subject =
      BehaviorSubject<IngredientResponse>();

  getIngredientVideos(String id) async {
    IngredientResponse response = await _repository.fetchIngredient(id);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<IngredientResponse> get subject => _subject;
}

final ingredientVideosBloc = IngredientsBloc();
