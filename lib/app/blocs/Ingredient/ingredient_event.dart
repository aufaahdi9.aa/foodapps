import 'package:equatable/equatable.dart';
import 'package:food_app/app/models/IngredientResponse.dart';

abstract class IngredientEvent extends Equatable {
  const IngredientEvent();
}

class GetIngredientList extends IngredientEvent {
  @override
  List<Object> get props => null;
}

abstract class IngredientState extends Equatable {
  const IngredientState();
}

class IngredientInitial extends IngredientState {
  const IngredientInitial();
  @override
  List<Object> get props => [];
}

class IngredientLoading extends IngredientState {
  const IngredientLoading();
  @override
  List<Object> get props => null;
}

class IngredientLoaded extends IngredientState {
  final IngredientResponse ingredientModel;
  const IngredientLoaded(this.ingredientModel);
  @override
  List<Object> get props => [ingredientModel];
}

class IngredientError extends IngredientState {
  final String message;
  const IngredientError(this.message);
  @override
  List<Object> get props => [message];
}
