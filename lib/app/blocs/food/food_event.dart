import 'package:equatable/equatable.dart';
import 'package:food_app/app/models/food.dart';

abstract class FoodEvent extends Equatable {
  const FoodEvent();
}

class GetFoodList extends FoodEvent {
  @override
  List<Object> get props => null;
}

abstract class FoodState extends Equatable {
  const FoodState();
}

class FoodInitial extends FoodState {
  const FoodInitial();
  @override
  List<Object> get props => [];
}

class FoodLoading extends FoodState {
  const FoodLoading();
  @override
  List<Object> get props => null;
}

class FoodLoaded extends FoodState {
  final FoodModel foodModel;
  const FoodLoaded(this.foodModel);
  @override
  List<Object> get props => [foodModel];
}

class FoodError extends FoodState {
  final String message;
  const FoodError(this.message);
  @override
  List<Object> get props => [message];
}
