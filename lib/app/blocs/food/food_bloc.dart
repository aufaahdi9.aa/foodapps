import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:food_app/app/blocs/food/food_event.dart';
import 'package:food_app/data/resources/api_repository.dart';

class FoodBloc extends Bloc<FoodEvent, FoodState> {
  final ApiRepository _apiRepository = ApiRepository();

  FoodBloc() : super(FoodInitial());

  @override
  Stream<FoodState> mapEventToState(
    FoodEvent event,
  ) async* {
    if (event is GetFoodList) {
      try {
        yield FoodLoading();
        final mList = await _apiRepository.fetchFoodList();
        yield FoodLoaded(mList);
        if (mList.error != null) {
          yield FoodError(mList.error);
        }
      } on NetworkError {
        yield FoodError("Failed to fetch data. is your device online?");
      }
    }
  }
}
