import 'package:moor_flutter/moor_flutter.dart';
part 'item_db.g.dart';

class Items extends Table {
  TextColumn get productImg => text()();
  IntColumn get id => integer().autoIncrement()();
  TextColumn get productName => text()();
  TextColumn get productCode => text()();
}


@UseMoor(tables: [Items])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
      path: "db.sqlite", logStatements: true));
  int get schemaVersion => 1;
  Future<List<Item>> getAllItem() => select(items).get();
  Stream<List<Item>> watchAllItem() => select(items).watch();
  Future insertNewItem(Item item) => into(items).insert(item);
  Future deleteItem(Item item) => delete(items).delete(item);
}