
part of 'item_db.dart';

class Item extends DataClass implements Insertable<Item> {
  final String productImg;
  final int id;
  final String productName;
  final String productCode;
  Item(
      {@required this.productImg,
      @required this.id,
      @required this.productName,
      @required this.productCode});
  factory Item.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    return Item(
      productImg: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}product_img']),
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      productName: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}product_name']),
      productCode: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}product_code']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || productImg != null) {
      map['product_img'] = Variable<String>(productImg);
    }
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || productName != null) {
      map['product_name'] = Variable<String>(productName);
    }
    if (!nullToAbsent || productCode != null) {
      map['product_code'] = Variable<String>(productCode);
    }
    return map;
  }

  ItemsCompanion toCompanion(bool nullToAbsent) {
    return ItemsCompanion(
      productImg: productImg == null && nullToAbsent
          ? const Value.absent()
          : Value(productImg),
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      productName: productName == null && nullToAbsent
          ? const Value.absent()
          : Value(productName),
      productCode: productCode == null && nullToAbsent
          ? const Value.absent()
          : Value(productCode),
    );
  }

  factory Item.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Item(
      productImg: serializer.fromJson<String>(json['productImg']),
      id: serializer.fromJson<int>(json['id']),
      productName: serializer.fromJson<String>(json['productName']),
      productCode: serializer.fromJson<String>(json['productCode']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'productImg': serializer.toJson<String>(productImg),
      'id': serializer.toJson<int>(id),
      'productName': serializer.toJson<String>(productName),
      'productCode': serializer.toJson<String>(productCode),
    };
  }

  Item copyWith(
          {String productImg,
          int id,
          String productName,
          String productCode}) =>
      Item(
        productImg: productImg ?? this.productImg,
        id: id ?? this.id,
        productName: productName ?? this.productName,
        productCode: productCode ?? this.productCode,
      );
  @override
  String toString() {
    return (StringBuffer('Item(')
          ..write('productImg: $productImg, ')
          ..write('id: $id, ')
          ..write('productName: $productName, ')
          ..write('productCode: $productCode')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(productImg.hashCode,
      $mrjc(id.hashCode, $mrjc(productName.hashCode, productCode.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Item &&
          other.productImg == this.productImg &&
          other.id == this.id &&
          other.productName == this.productName &&
          other.productCode == this.productCode);
}

class ItemsCompanion extends UpdateCompanion<Item> {
  final Value<String> productImg;
  final Value<int> id;
  final Value<String> productName;
  final Value<String> productCode;
  const ItemsCompanion({
    this.productImg = const Value.absent(),
    this.id = const Value.absent(),
    this.productName = const Value.absent(),
    this.productCode = const Value.absent(),
  });
  ItemsCompanion.insert({
    @required String productImg,
    this.id = const Value.absent(),
    @required String productName,
    @required String productCode,
  })  : productImg = Value(productImg),
        productName = Value(productName),
        productCode = Value(productCode);
  static Insertable<Item> custom({
    Expression<String> productImg,
    Expression<int> id,
    Expression<String> productName,
    Expression<String> productCode,
  }) {
    return RawValuesInsertable({
      if (productImg != null) 'product_img': productImg,
      if (id != null) 'id': id,
      if (productName != null) 'product_name': productName,
      if (productCode != null) 'product_code': productCode,
    });
  }

  ItemsCompanion copyWith(
      {Value<String> productImg,
      Value<int> id,
      Value<String> productName,
      Value<String> productCode}) {
    return ItemsCompanion(
      productImg: productImg ?? this.productImg,
      id: id ?? this.id,
      productName: productName ?? this.productName,
      productCode: productCode ?? this.productCode,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (productImg.present) {
      map['product_img'] = Variable<String>(productImg.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (productName.present) {
      map['product_name'] = Variable<String>(productName.value);
    }
    if (productCode.present) {
      map['product_code'] = Variable<String>(productCode.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ItemsCompanion(')
          ..write('productImg: $productImg, ')
          ..write('id: $id, ')
          ..write('productName: $productName, ')
          ..write('productCode: $productCode')
          ..write(')'))
        .toString();
  }
}

class $ItemsTable extends Items with TableInfo<$ItemsTable, Item> {
  final GeneratedDatabase _db;
  final String _alias;
  $ItemsTable(this._db, [this._alias]);
  final VerificationMeta _productImgMeta = const VerificationMeta('productImg');
  GeneratedTextColumn _productImg;
  @override
  GeneratedTextColumn get productImg => _productImg ??= _constructProductImg();
  GeneratedTextColumn _constructProductImg() {
    return GeneratedTextColumn(
      'product_img',
      $tableName,
      false,
    );
  }

  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _productNameMeta =
      const VerificationMeta('productName');
  GeneratedTextColumn _productName;
  @override
  GeneratedTextColumn get productName =>
      _productName ??= _constructProductName();
  GeneratedTextColumn _constructProductName() {
    return GeneratedTextColumn(
      'product_name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _productCodeMeta =
      const VerificationMeta('productCode');
  GeneratedTextColumn _productCode;
  @override
  GeneratedTextColumn get productCode =>
      _productCode ??= _constructProductCode();
  GeneratedTextColumn _constructProductCode() {
    return GeneratedTextColumn(
      'product_code',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [productImg, id, productName, productCode];
  @override
  $ItemsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'items';
  @override
  final String actualTableName = 'items';
  @override
  VerificationContext validateIntegrity(Insertable<Item> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('product_img')) {
      context.handle(
          _productImgMeta,
          productImg.isAcceptableOrUnknown(
              data['product_img'], _productImgMeta));
    } else if (isInserting) {
      context.missing(_productImgMeta);
    }
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('product_name')) {
      context.handle(
          _productNameMeta,
          productName.isAcceptableOrUnknown(
              data['product_name'], _productNameMeta));
    } else if (isInserting) {
      context.missing(_productNameMeta);
    }
    if (data.containsKey('product_code')) {
      context.handle(
          _productCodeMeta,
          productCode.isAcceptableOrUnknown(
              data['product_code'], _productCodeMeta));
    } else if (isInserting) {
      context.missing(_productCodeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Item map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Item.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ItemsTable createAlias(String alias) {
    return $ItemsTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $ItemsTable _items;
  $ItemsTable get items => _items ??= $ItemsTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [items];
}
