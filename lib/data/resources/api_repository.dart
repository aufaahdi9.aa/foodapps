import 'package:food_app/app/models/IngredientResponse.dart';
import 'package:food_app/app/models/food.dart';
import 'package:food_app/app/utils/api_provider.dart';

class ApiRepository {
  final _provider = ApiProvider();

  Future<FoodModel> fetchFoodList() {
    return _provider.fetchFoodList();
  }

  Future<IngredientResponse> fetchIngredient(String id) {
    return _provider.fetchIngredient(id);
  }
}

class NetworkError extends Error {}
